# Objectif du projet Jeu de cartes

L'objectif de ce projet est :

- de créer un **serveur** qui se sert de l'API : https://deckofcardsapi.com/, accessible via HTTP, après démarrage
- de créer un **client** qui se sert du **serveur**

# Organisation du code

Ce dépôt présente deux dossiers : un pour le serveur et un pour le client. Pour pouvoir utiliser le client il est **nécessaire** de lancer le serveur dans un premier temps.

# Quickstart

### Etape 1 : lancer le serveur

A la racine du projet, dans un terminal, lancer les commandes suivantes :

- `cd webservice`
- `pip3 install -r requirements.txt`
- `uvicorn main:app --reload`

### Etape 2 : lancer le client

A la racine du projet, dans un **autre** terminal, lancer les commandes suivantes :

- `cd client`
- `pip3 install -r requirements.txt`
- `python main.py` pour lancer l'interface (si une erreur se produit lancez `python3 main.py`)
- `python scenario.py`pour lancer le scénario (si une erreur se produit lancez `python3 scenario.py`)
- `pytest` pour lancer les tests
