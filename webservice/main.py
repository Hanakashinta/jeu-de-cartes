from fastapi import FastAPI
import requests
from pydantic import BaseModel

app = FastAPI()

def read_deck():
    r=requests.get("https://deckofcardsapi.com/api/deck/new/shuffle")
    r_json = r.json()
    return r_json

def read_cards_from_deck(deck_id:str, n_cards:int):
    string_n_cards = "{}".format(n_cards)
    r=requests.get("https://deckofcardsapi.com/api/deck/"+deck_id+"/draw/?count="+string_n_cards)
    r_json = r.json()
    return r_json


class Deck(BaseModel):
    deck_id: str
    remaining: int

@app.get("/")
async def root():
    return {"message": "Bienvenue!"}

@app.get("/creer-un-deck/")
async def create_deck():
    r_json = read_deck()
    return r_json

@app.post("/cartes/{nombre_cartes}")
async def create_pile(nombre_cartes:int, deck:dict):
    r_json = read_cards_from_deck(deck["deck_id"], nombre_cartes)
    return r_json