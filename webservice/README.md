# Présentation du webservice

Le webservice permet de :

- récupérer un deck de l'api et renvoie son id
- récupérer un nombre x de cartes d'un deck

# Fonctionnement du code

Pour coder le webservice on utilise les paquets `fastapi` et `requests`.

Pour lancer le webservice on utilise le paquet `uvicorn`.

Le code présente un unique fichier python main.py
Dans ce fichier sont créés :

- des méthodes qui effectuent des requêtes à l'API https://deckofcardsapi.com/ en utilisant fastapi et requests
- une méthode exposée en `GET` sur `/creer-un-deck/`
- une méthode exposée en `POST` sur `/cartes/{nombre_cartes}` avec un body : `{deck_id:str}`

# Quickstart

Dans un terminal, à la racine du projet, lancer les commandes suivantes :

- `cd webservice`
- `pip3 install -r requirements.txt`
- `uvicorn main:app --reload`
