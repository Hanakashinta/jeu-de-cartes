

from service.service import Service

def test_afficher_cartes():
    pile_of_cards = {
        "deck_id": "6p79q6tfdmom",
        "cards": [
            {
            "code": "KH",
            "image": "https://deckofcardsapi.com/static/img/KH.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/KH.svg",
                "png": "https://deckofcardsapi.com/static/img/KH.png"
            },
            "value": "KING",
            "suit": "HEARTS"
            },
            {
            "code": "AC",
            "image": "https://deckofcardsapi.com/static/img/AC.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/AC.svg",
                "png": "https://deckofcardsapi.com/static/img/AC.png"
            },
            "value": "ACE",
            "suit": "CLUBS"
            },
            {
            "code": "6C",
            "image": "https://deckofcardsapi.com/static/img/6C.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/6C.svg",
                "png": "https://deckofcardsapi.com/static/img/6C.png"
            },
            "value": "6",
            "suit": "CLUBS"
            },
            {
            "code": "AD",
            "image": "https://deckofcardsapi.com/static/img/aceDiamonds.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/aceDiamonds.svg",
                "png": "https://deckofcardsapi.com/static/img/aceDiamonds.png"
            },
            "value": "ACE",
            "suit": "DIAMONDS"
            },
            {
            "code": "5S",
            "image": "https://deckofcardsapi.com/static/img/5S.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/5S.svg",
                "png": "https://deckofcardsapi.com/static/img/5S.png"
            },
            "value": "5",
            "suit": "SPADES"
            },
            {
            "code": "8D",
            "image": "https://deckofcardsapi.com/static/img/8D.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/8D.svg",
                "png": "https://deckofcardsapi.com/static/img/8D.png"
            },
            "value": "8",
            "suit": "DIAMONDS"
            },
            {
            "code": "3H",
            "image": "https://deckofcardsapi.com/static/img/3H.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/3H.svg",
                "png": "https://deckofcardsapi.com/static/img/3H.png"
            },
            "value": "3",
            "suit": "HEARTS"
            },
            {
            "code": "6H",
            "image": "https://deckofcardsapi.com/static/img/6H.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/6H.svg",
                "png": "https://deckofcardsapi.com/static/img/6H.png"
            },
            "value": "6",
            "suit": "HEARTS"
            },
            {
            "code": "0H",
            "image": "https://deckofcardsapi.com/static/img/0H.png",
            "images": {
                "svg": "https://deckofcardsapi.com/static/img/0H.svg",
                "png": "https://deckofcardsapi.com/static/img/0H.png"
            },
            "value": "10",
            "suit": "HEARTS"
            }
        ],
        "remaining": 43
        }
    affichage = Service.afficher_cartes(pile_of_cards)
    assert affichage == "KING HEARTS\nACE CLUBS\n6 CLUBS\nACE DIAMONDS\n5 SPADES\n8 DIAMONDS\n3 HEARTS\n6 HEARTS\n10 HEARTS\n"

def test_couleur():
    liste_cartes = [
    {
      "code": "KH",
      "image": "https://deckofcardsapi.com/static/img/KH.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/KH.svg",
        "png": "https://deckofcardsapi.com/static/img/KH.png"
      },
      "value": "KING",
      "suit": "HEARTS"
    },
    {
      "code": "AC",
      "image": "https://deckofcardsapi.com/static/img/AC.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/AC.svg",
        "png": "https://deckofcardsapi.com/static/img/AC.png"
      },
      "value": "ACE",
      "suit": "CLUBS"
    },
    {
      "code": "6C",
      "image": "https://deckofcardsapi.com/static/img/6C.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/6C.svg",
        "png": "https://deckofcardsapi.com/static/img/6C.png"
      },
      "value": "6",
      "suit": "CLUBS"
    },
    {
      "code": "AD",
      "image": "https://deckofcardsapi.com/static/img/aceDiamonds.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/aceDiamonds.svg",
        "png": "https://deckofcardsapi.com/static/img/aceDiamonds.png"
      },
      "value": "ACE",
      "suit": "DIAMONDS"
    },
    {
      "code": "5S",
      "image": "https://deckofcardsapi.com/static/img/5S.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/5S.svg",
        "png": "https://deckofcardsapi.com/static/img/5S.png"
      },
      "value": "5",
      "suit": "SPADES"
    },
    {
      "code": "8D",
      "image": "https://deckofcardsapi.com/static/img/8D.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/8D.svg",
        "png": "https://deckofcardsapi.com/static/img/8D.png"
      },
      "value": "8",
      "suit": "DIAMONDS"
    },
    {
      "code": "3H",
      "image": "https://deckofcardsapi.com/static/img/3H.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/3H.svg",
        "png": "https://deckofcardsapi.com/static/img/3H.png"
      },
      "value": "3",
      "suit": "HEARTS"
    },
    {
      "code": "6H",
      "image": "https://deckofcardsapi.com/static/img/6H.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/6H.svg",
        "png": "https://deckofcardsapi.com/static/img/6H.png"
      },
      "value": "6",
      "suit": "HEARTS"
    },
    {
      "code": "0H",
      "image": "https://deckofcardsapi.com/static/img/0H.png",
      "images": {
        "svg": "https://deckofcardsapi.com/static/img/0H.svg",
        "png": "https://deckofcardsapi.com/static/img/0H.png"
      },
      "value": "10",
      "suit": "HEARTS"
    }
  ]
    repartition = Service.couleur(liste_cartes)
    assert repartition == {"H": 4, "S" : 1, "C" : 2, "D" : 2}