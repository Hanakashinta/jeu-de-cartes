

from service.service import Service

if __name__ == "__main__":
    value = True
    while value :

        # création du deck : renvoie un fichier json
        deck = Service.create_deck()
        print("L'id du nouveau deck est : {} \n".format(deck["deck_id"]))

        # création d'une pile de carte tirées : renvoie un fichier json 
        pile_of_ten_cards = Service.create_pile(deck, 10)

        # affiche les cartes tirées
        display_of_ten_cards = Service.afficher_cartes(pile_of_ten_cards)
        print("10 cartes tirées du deck : \n{}".format(display_of_ten_cards))

        # liste des cartes tirées : liste
        list_of_ten_cards = pile_of_ten_cards["cards"]

        # couleur des cartes tirées 
        dictionnaire_couleur = Service.couleur(list_of_ten_cards)
        print("Les couleurs des 10 cartes tirées sont : {}\n".format(dictionnaire_couleur))

        print("Voilà pour le scénario !\n")

        value = False
