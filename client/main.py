# coding: utf8
from view.welcome_view import WelcomeView

if __name__ == "__main__":
    current_view = WelcomeView()

    with open('assets/welcome.txt', 'r', encoding="utf-8") as asset:
        print(asset.read())

    while current_view:

        # les infos à afficher
        current_view.display_info()
        # le choix que doit saisir l'utilisateur
        current_view = current_view.make_choice()

        # bordure qui sépare les view
        with open('assets/separator.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())
    
    with open('assets/end.txt', 'r', encoding="utf-8") as asset:
        print(asset.read())