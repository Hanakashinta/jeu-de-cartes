import requests

class Service :

    def create_deck():

        #renvoie un dictionnaire

        r=requests.get("http://127.0.0.1:8000/creer-un-deck/")
        r_json=r.json()
        return {"deck_id": r_json["deck_id"], "remaining":r_json["remaining"]}
   
    def create_pile(deck, nombre_cartes):

        # renvoie un dictionnaire        
        
        string_nombre_cartes = "{}".format(nombre_cartes)
        r=requests.post("http://127.0.0.1:8000/cartes/"+string_nombre_cartes, json=deck )
        r_json = r.json()
        return {"deck_id" : r_json["deck_id"], "remaining" : r_json["remaining"], "cards":r_json["cards"]}
    
    def afficher_cartes(pile):
        
        # renvoie string

        cartes = pile["cards"]
        n_cartes = len(cartes)
        affichage = ""
        for i in range(0, n_cartes):
            affichage += "{} {}\n".format(cartes[i]["value"], cartes[i]["suit"])
        
        return affichage

    def couleur(liste_cartes):

        # renvoie un dictionnaire

        taille_liste = len(liste_cartes)
        
        n_hearts=0
        n_spades=0
        n_clubs=0
        n_diamonds=0

        for i in range(0,taille_liste):
            if liste_cartes[i]["suit"] == "HEARTS":
                n_hearts +=1
            elif liste_cartes[i]["suit"] == "SPADES":
                n_spades +=1
            elif liste_cartes[i]["suit"] == "CLUBS":
                n_clubs +=1
            else :
                n_diamonds += 1
                
        couleur = {"H":n_hearts, "S" : n_spades, "D" : n_diamonds, "C" : n_clubs}
        
        return couleur