# Présentation du client

Le client permet de :

- lancer une interface qui propose de :

  - tirer un deck mélangé
  - tirer des cartes de ce deck et de les affichers
  - donner la répartition des couleurs des cartes tirées

- lancer un scénario qui tire automatiquement 10 cartes d'un deck mélangé et donne la répartition de ces cartes

# Fonctionnement du code

Pour coder le client, on utilise les paquets `requests`, `PyInquirer`.

Le code présente plusieurs modules :

- assests : contient des fichiers .txt à afficher dans l'interface
- service : contient un fichier service.py qui contient des méthodes qui permettent de :
  - générer un nouveau deck en faisait un requête GET au webservice
  - générer une pile de cartes du deck courant en faisant un requête POST au webservice
  - renvoyer les cartes tirées
  - renvoyer la répartition des couleurs de toutes les cartes tirées d'un deck
- test : effectuent des tests sur des méthodes de la classe Service
- view : contient les fichiers d'affichages dans la console

  et les fichiers :

- main : permet d'exécuter l'interface
- scenario : permet d'exécuter le scénario

# Quickstart

Après avoir lancer le webservice, dans un **autre** terminal, à la racine du projet, lancer les commandes suivantes :

- `cd client`
- `pip3 install -r requirements.txt`
- `python main.py` pour lancer l'interface (si une erreur se produit lancez `python3 main.py`)
- `python scenario.py`pour lancer le scénario (si une erreur se produit lancez `python3 scenario.py`)

# Lancer les tests

Dans le dossier `client`, lancez la commande suivante : `pytest`
