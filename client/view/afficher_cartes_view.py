


from PyInquirer import prompt

from view.abstract_view import AbstractView

from service.service import Service

class AfficherCartesView(AbstractView):

    def __init__(self, deck, liste_cartes, nombre_cartes):

        # récupère le deck et la pile (une liste de cartes tirées du deck) et le nombre de cartes à tirer 
        self.deck = deck
        self.liste_cartes = liste_cartes
        self.nombre_cartes = nombre_cartes

        # tire des cartes du deck
        self.pile = Service.create_pile(self.deck, self.nombre_cartes)

        # maj de la liste des cartes tirées au total
        self.liste_cartes = self.liste_cartes + self.pile["cards"]

        # maj des cartes restantes dans le deck 
        self.cartes_restantes = self.pile["remaining"]
        self.deck["remaining"] = self.pile["remaining"]

        # id du deck
        self.deck_id = self.pile["deck_id"]

        # si il n'y a plus de cartes dans le deck on ne propose pas de tirer de nouveau des cartes
        if self.deck["remaining"] == 0 :
            choix = [
                "Nombre de cartes de chaque couleur des cartes tirées",
                "Menu principal"
                ]
        
        else :

            choix = [
                    "Tirer des cartes",
                    "Nombre de cartes de chaque couleur des cartes tirées",
                    "Menu principal"
                ]

        # création du menu
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous faire ?',
                'choices': choix
            }
        ]

    def display_info(self):
        print("Vous voulez tirer {} cartes".format(self.nombre_cartes))
        print("")

        print("Vous avez tiré les cartes :")
        print(Service.afficher_cartes(self.pile))
        print("Il reste {} cartes dans le deck {}\n".format(self.cartes_restantes,self.deck_id))


    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["choix"] == "Menu principal":
            from view.welcome_view import WelcomeView
            return WelcomeView()

        elif reponse["choix"] == "Tirer des cartes":
            from view.tirer_cartes_view import TirerCartesView
            return TirerCartesView(self.deck, self.liste_cartes)
        
        elif reponse["choix"] == "Nombre de cartes de chaque couleur des cartes tirées":
            couleur = Service.couleur(self.liste_cartes)
            print(couleur)
            from view.creer_deck_view import CreerDeckView
            return CreerDeckView(self.deck, self.liste_cartes)