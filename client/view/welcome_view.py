

from PyInquirer import prompt
from view.abstract_view import AbstractView


class WelcomeView(AbstractView):

    def __init__(self):
        """
        création du menu principal
        """

        # intiialisation d'un deck à None
        self.deck = None
        self.liste_cartes = []

        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous faire ?',
                'choices': [
                    "Créer un nouveau deck",
                    "Quitter l'application"
                ]
            }
        ]

    def make_choice(self):
        reponse = prompt(self.questions)
        if reponse['choix'] == "Créer un nouveau deck":
            from view.creer_deck_view import CreerDeckView
            return CreerDeckView(self.deck, self.liste_cartes)

        if reponse["choix"] == "Quitter l'application":
            return None

    def display_info(self):
        pass
