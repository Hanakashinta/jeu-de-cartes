

from PyInquirer import prompt

from view.abstract_view import AbstractView

from view.afficher_cartes_view import AfficherCartesView

class TirerCartesView(AbstractView):

    def __init__(self, deck, liste_cartes):

        self.deck = deck
        self.liste_cartes = liste_cartes
        self.cartes_restantes = self.deck["remaining"]

        nombre_cartes_dispo = ["Menu principal"]

        nombre_cartes_dispo = nombre_cartes_dispo + ["{}".format(i) for i in range (0,self.cartes_restantes+1)]
        

        # création du menu
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Combien de cartes voulez-vous tirer ?',
                'choices': nombre_cartes_dispo
            }
        ]

    def display_info(self):
        pass

    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["choix"] == "Menu principal":
            from view.welcome_view import WelcomeView
            return WelcomeView()

        elif reponse["choix"] == "0":
            from view.creer_deck_view import CreerDeckView
            return CreerDeckView(self.deck, self.liste_cartes)

        else:
            return AfficherCartesView(self.deck, self.liste_cartes, reponse["choix"])
