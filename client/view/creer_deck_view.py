

from PyInquirer import prompt

from view.abstract_view import AbstractView

from service.service import Service

from view.tirer_cartes_view import TirerCartesView


class CreerDeckView(AbstractView):

    def __init__(self, deck, liste_cartes):

        self.deck = deck
        self.liste_cartes = liste_cartes
        
        # si pas de deck courant :
        if self.deck == None :
            self.deck = Service.create_deck()
        
        self.cartes_restantes = self.deck["remaining"]
        if self.cartes_restantes == 0 :
            liste_choix = ["Menu principal"]
        
        else :
            liste_choix = [
                    "Tirer des cartes",
                    "Menu principal"
                ]


        # création du menu
        self.questions = [
            {
                'type': 'list',
                'name': 'choix',
                'message': 'Que voulez-vous faire ?',
                'choices': liste_choix
            }
        ]

    def display_info(self):
        deck_id = self.deck["deck_id"]
        print("L'id du deck courant est : {}".format(deck_id))
        print("")

        if self.cartes_restantes == 0 :
            print("Il n'y a plus de cartes dans ce deck. Vous pouvez seulement retourner au menu principal... :p")
            print("")


    def make_choice(self):
        reponse = prompt(self.questions)

        if reponse["choix"] == "Menu principal":
            from view.welcome_view import WelcomeView
            return WelcomeView()

        else:
            return TirerCartesView(self.deck, self.liste_cartes)
